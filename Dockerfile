FROM arm64v8/wordpress:5-php8.1-fpm-alpine
ENV PHP_FPM_PM="dynamic"
ENV PHP_FPM_MAX_CHILDREN="5"
ENV PHP_FPM_START_SERVERS="2"
ENV PHP_FPM_MIN_SPARE_SERVERS="1"
ENV PHP_FPM_MAX_SPARE_SERVERS="2"
ENV PHP_FPM_MAX_REQUESTS="1000"
COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf
WORKDIR /usr/src/wordpress
#RUN set -eux; \
#    find /etc/apache2 -name '*.conf' -type f -exec sed -ri -e "s!/var/www/html!$PWD!g" -e "s!Directory /var/www/!Directory $PWD!g" '{}' +; \
#    cp -s wp-config-docker.php wp-config.php
#COPY custom-theme/ ./wp-content/themes/custom-theme/
#COPY custom-plugin/ ./wp-content/plugins/custom-plugin/
